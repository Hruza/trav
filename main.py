﻿#!/usr/bin/python
# coding: UTF-8
import mechanize
from mechanize import ParseResponse
from bs4 import BeautifulSoup
from sys import stdout, stderr

__author__ = 'x'

SILENT = 0


def info(msg):
    if not SILENT:
        stdout.write('[>] %s\n' % msg)


def error(msg):
    if not SILENT:
        stderr.write('[!] %s\n' % msg)


class LoginError(Exception):
    def __init__(self, value):
        self.param = value

    def __str__(self):
        return 'LoginError: ', str(self.param)


def navigate(url):
    if type(url) in (str, unicode):
        url = baseurl + url
    return mechanize.urlopen(url)

#
server = 'ts5'
username = 'uranic.market@gmail.com'
password = 'okoloa'
baseurl = 'http://%s.travian.cz/' % server

# prihlaseni k aplikaci
info("Logging into %s.travian.cz as %s..." % (server, username))
soup = BeautifulSoup(navigate(''))
fields = soup.find('form', {'name': 'login'}).findAll('input')
fieldu = fields[0]['name']
fieldp = fields[1]['name']

forms = ParseResponse(navigate(''), backwards_compat=False)
form = forms[0]
form[fieldu] = username
form[fieldp] = password

resp = navigate(form.click('s1'))

if resp.read().find("Password forgotten") != -1:
    raise LoginError('Login failed. Username or password incorrect.')
else:
    info('Login successful.')
coord = (49, -157)
info("Adding village [%s,%s] to farmlist..." % (coord[0], coord[1]))
soup = BeautifulSoup(navigate(''))
soup = BeautifulSoup(navigate('build.php?gid=16&tt=99&action=showSlot&lid=5006&sort=distance&direction=asc'))
fields = soup.find('form', {'id': 'edit_form'}).findAll('input')
field_x = fields[3]['name']
field_y = fields[4]['name']
field_blesk = fields[8]['name']

forms = ParseResponse(navigate('build.php?gid=16&tt=99&action=showSlot&lid=5006&sort=distance&direction=asc'),
                      backwards_compat=False)
form = forms[0]
form[field_x] = str(coord[0])
form[field_y] = str(coord[1])
form[field_blesk] = '20'
resp = navigate(form.click('save'))

if resp.read().find("Na zadaných souřadnicích není žádná vesnice!") != -1:
    raise Exception('Na zadaných souřadnicích není žádná vesnice!')
else:
    info('Added.')
