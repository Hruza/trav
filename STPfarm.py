#!/usr/bin/python
# coding: UTF-8
from bs4 import BeautifulSoup
import itertools
import choice
from sys import stdout

__author__ = 'stp'

import travian

username = 'xxx'
password = 'xxx'
filename = 'coords.txt'
SILENT = 0

def get_coords(filename):
    return map(lambda x: eval(x), filter(lambda x: x, [line.strip().replace('|', ',') for line in open(filename)]))
def info(msg):
    if not SILENT:
        stdout.write('[>] %s\n' % msg)

coords = get_coords("coords.txt")

#for souradnice in filter(lambda x: x, [line.strip().replace('|', ',') for line in open(filename)]):
#    info(souradnice)
#    eval(souradnice)


#farmlist = travian.Farmlist(travian.Connection())
# for coord in coords:
#     farmlist.add_village(coord)
#farmlist.remove_village(None)

import mechanize

br = mechanize.Browser()
# br.set_all_readonly(False)    # allow everything to be written to
# br.set_handle_robots(False)   # ignore robots
# br.set_handle_refresh(False)  # can sometimes hang without this
#br.addheaders =             # [('User-agent', 'Firefox')]

response = br.open('http://ts5.travian.cz/')
br.select_form(nr=0)
br.form['name'] = username
br.form['password'] = password
response = br.click('s1')
br.submit()



response = br.open('http://ts5.travian.cz/build.php?tt=99&id=39')
bf = BeautifulSoup(response)
lids = []
lists = []
forms = bf.findAll('form')
forms.remove(forms[len(forms) - 1])
for form in forms:
    inputTag = form.find('input', {'name': 'lid'})
    divTag = form.find('div', {'class': 'listTitleText'})
    lists.append(
        (inputTag.get('value'), next(itertools.islice(divTag.children, 2, 3)).replace('\n', '').replace('\t', '')))

result = choice.Menu(lists, title='Select farmlist:').ask()
print result