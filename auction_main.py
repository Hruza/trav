﻿#!/usr/bin/python
# coding=utf-8
from bs4 import BeautifulSoup
from mechanize import FormNotFoundError
import choice
import travian

__author__ = 'xxx'


# username = 'xxx'
# password = 'xxx'


def get_coords(filename):
    return map(lambda x: eval(x), filter(lambda x: x, [line.strip().replace('|', ',') for line in open(filename)]))


import mechanize

# username = choice.Input("Zadejte uzivatelske jmeno", str).ask()
# password = choice.Input("Zadejte heslo", str).ask()

userList = [('xxx', 'xxx'), ('xxx', 'xxx')]
userMap = dict(userList)
password = choice.Menu(userList, title='Vyber uzivatele').ask()
username = userMap[password]

br = mechanize.Browser()

response = br.open('http://ts5.travian.cz/')
br.select_form(nr=0)
br.form['name'] = username
br.form['password'] = password
response = br.click('s1')
br.submit()


def isauctiondetail(form):
    return 'id' in form.attrs and form.attrs['id'].startswith('auctionDetails')

#Musíš nabídnout minimálně

# NASTAVENI
prizePerItem = 1   # cena za kus
minBid = 2 # min. prihoz
maxBin = 10000         # max. prihoz
maximumCost = 10000    # max. stribrnych
maxQuantity = 500       # max. pocet kusu jedne nabidky

itemsList = [(7, u"Náplast"), (8, u"Obvaz"), (9, u"Klec"), (11, u"Bylinková mast"), (14, u"Tabulka zákona"),
             (15, u'Umělecké dílo')]
itemsMap = dict(itemsList)
selectedItem = choice.Menu(itemsList, title="Vyberte predmet:").ask()


def bidpage(pageurl, actualcost):
    auctionresponse = BeautifulSoup(br.open(pageurl))
    _lasthash = None
    for trTag in auctionresponse.findAll("table")[0].find('tbody').findAll('tr'):
        namecell = trTag.find('td', {'class': 'name'}).get_text().strip()
        #ziskam radek tabulky s item
        namecell = namecell.strip()
        splited = namecell.split(u' ')
        #z textu sloupecku zjistim typ itemu

        itemquantity = int(splited[0].encode('ascii', 'ignore'))
        splited.pop(0)
        itemtype = u" ".join(splited).strip()
        if itemtype == itemsMap[selectedItem] and itemquantity <= maxQuantity:
            # ziskame sloupecek s casem
            timecell = next(trTag.find('td', {'class': 'time'}).children).get_text()
            # ziskame aktualni cenu
            silver = int(next(trTag.find('td', {'class': 'silver'}).children))
            # vypocteme nami nabizenou cenu
            countbid = itemquantity * prizePerItem
            if countbid < minBid:
                countbid = minBid
            if countbid >= silver:
                try:
                    # travian.info("%dx %s [%d] konec v %s)..." % (itemquantity, itemtype, silver, timecell))
                    text = next(trTag.find('a').children).strip()
                    if text == u"Změnit":
                        # travian.info("uz je prihozeno")
                        continue

                    href = trTag.find('a').get('href')
                    if _lasthash is not None:
                        #zmeni v odkazu hash, kterou mame z predesleho odeslani formulare
                        href = href[:-len(href) + href.find(u"z=")] + u"z=" + _lasthash
                    br.open('http://ts5.travian.cz/' + href)
                    br.select_form(predicate=isauctiondetail)

                    if countbid < maxBin and actualcost + countbid <= maximumCost:
                        br.form['maxBid'] = str(countbid)
                        bf = BeautifulSoup(br.submit())
                        error = bf.find('div', {'class': 'error'})
                        if error:
                            travian.error(next(error.children).strip())
                        else:
                            actualcost += countbid
                            # travian.info("prihozeno %d" % countbid)
                            # zjistime hash z nove nactene stranky
                        _trTag = bf.findAll("table")[0].find('tbody').findAll('tr')
                        _hashFound = False
                        for _tr in _trTag:
                            _aTag = _tr.find('a')
                            if _aTag is not None:
                                href = _aTag.get('href')
                                _lasthash = href[href.find(u"z=") + 2:]
                                _hashFound = True
                                break
                        if not _hashFound:
                            travian.error(u'nepodarilo se najit hash pro nasledujici odkaz!')
                except FormNotFoundError:
                    return actualcost, False, True
                    pass
    # pokud jsme na posledni strance koncime
    if auctionresponse.find('img', {'class': 'last disabled'}) is not None:
        return actualcost, True, False

    # jinak vracime pocet utracenych stribrnych
    return actualcost, False, False


total = 0
# projdeme vsechny stranky aukce
for pageNumber in xrange(1, 100):
    #travian.info("Otevrena stranka %s" % pageUrl)
    _bidUrl = 'http://ts5.travian.cz/hero_auction.php?filter=' + str(selectedItem) + '&page=' + str(pageNumber)
    result = bidpage(_bidUrl, total)
    # dokud se to nepovede, tak dotaz opakujeme
    while result[2]:
        result = bidpage(_bidUrl, total)

    total = result[0]
    # pokud jsme na posledni strance tak koncime
    if total >= maximumCost or result[1]:
        break
travian.info('Utraceno %d' % total)
























