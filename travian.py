﻿#!/usr/bin/python
# coding: UTF-8

import mechanize
from mechanize import ParseResponse
from bs4 import BeautifulSoup
from sys import stdout, stderr

__author__ = 'xxx'

SILENT = 0


def info(msg):
    if not SILENT:
        stdout.write('[>] %s\n' % msg)


def error(msg):
    if not SILENT:
        stderr.write('[!] %s\n' % msg.encode('ascii', errors='ignore'))


class LoginError(Exception):
    def __init__(self, value):
        self.param = value

    def __str__(self):
        return 'LoginError: ', str(self.param)


server = 'ts5'
username = 'xxx'
password = 'xxx'


class Connection:
    def __init__(self):
        self.baseurl = 'http://%s.travian.cz/' % server
        self.login()
        pass

    def navigate(self, url):
        if type(url) in (str, unicode):
            url = self.baseurl + url
        return mechanize.urlopen(url)

    # prihlaseni k aplikaci
    def login(self):
        info("Logging into %s.travian.cz as %s..." % (server, username))
        soup = BeautifulSoup(self.navigate(''))
        fields = soup.find('form', {'name': 'login'}).findAll('input')
        fieldu = fields[0]['name']
        fieldp = fields[1]['name']

        forms = ParseResponse(self.navigate(''), backwards_compat=False)
        form = forms[0]
        form[fieldu] = username
        form[fieldp] = password

        resp = self.navigate(form.click('s1'))

        if resp.read().find("Password forgotten") != -1:
            raise LoginError('Login failed. Username or password incorrect.')
        else:
            info('Login successful.')


class Farmlist:
    def __init__(self, travian):
        self.travian = travian
        # zjistime inputy pro x,y a blesky
        soup = BeautifulSoup(
            travian.navigate('build.php?gid=16&tt=99&action=showSlot&lid=5006&sort=distance&direction=asc'))
        fields = soup.find('form', {'id': 'edit_form'}).findAll('input')
        self.field_x = fields[3]['name']
        self.field_y = fields[4]['name']
        self.field_blesk = fields[8]['name']

        # nacteme si stranku farmlistu
        self.buil_page = BeautifulSoup(travian.navigate('build.php?tt=99&id=39'))
        pass

    # Pridani vesnice do farmlistu
    def add_village(self, coord):
        info("Adding village [%s,%s] to farmlist..." % (coord[0], coord[1]))
        forms = ParseResponse(
            self.travian.navigate('build.php?gid=16&tt=99&action=showSlot&lid=5006&sort=distance&direction=asc'),
            backwards_compat=False)
        form = forms[0]
        form[self.field_x] = str(coord[0])
        form[self.field_y] = str(coord[1])
        form[self.field_blesk] = '20'
        resp = self.travian.navigate(form.click('save'))

        if resp.read().find("Na zadaných souřadnicích není žádná vesnice!") != -1:
            error('No village found on (%s,%s)!' % (coord[0], coord[1]))
        else:
            info('Added.')

    # Odebrani vesnice z farmlistu
    def remove_village(self, coord):
 #       info("Removing village [%s,%s] from farmlist..." % (coord[0], coord[1]))
        forms = ParseResponse(self.travian.navigate('build.php?tt=99&id=39'), backwards_compat=False)
        form = forms[1]
        resp = self.travian.navigate(form.click('did100515'))
        # forms = ParseResponse(
        #     self.travian.navigate('build.php?gid=16&tt=99&action=showSlot&lid=5006&sort=distance&direction=asc'),
        #     backwards_compat=False)
        # form = forms[0]
        # form[self.field_x] = str(coord[0])
        # form[self.field_y] = str(coord[1])
        # form[self.field_blesk] = '20'
        # resp = self.travian.navigate(form.click('save'))
        #
        # if resp.read().find("Na zadaných souřadnicích není žádná vesnice!") != -1:
        #     error('No village found on (%s,%s)!' % (coord[0], coord[1]))
        # else:
        #     info('Added.')

