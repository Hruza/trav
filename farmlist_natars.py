from sys import stdout
import math

__author__ = 'stepanek'

SILENT = 0

def get_coords(filename):
    return map(lambda x: eval(x), filter(lambda x: x, [line.strip().replace('|', ',') for line in open(filename)]))
def info(msg):
    if not SILENT:
        stdout.write('[>] %s\n' % msg)

coords = get_coords("natars.txt")

def get_distance(coord):
    return math.sqrt(math.pow((coord[0] - 51), 2) + math.pow((coord[1] + 163), 2))

coords.sort(key=get_distance)
for coor in coords:
    print(coor)